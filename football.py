from common import Analyser
from common import Extractor


class FootballAnalyser(Analyser):
    """analyse the football data"""
    def __init__(self, file):
        self.data = Extractor.extract_data(file)
        self.rmv = ('-', '')
        self.col = (-3, -2, 1)


soccer_english_premier_league = FootballAnalyser("football.dat")
print(soccer_english_premier_league.min_diff())
