This project is to understand the SOLID principals.

SOLID says:

    S - Single-responsiblity principle.
    O - Open-closed principle.
    L - Liskov substitution principle.
    I - Interface segregation principle.
    D - Dependency Inversion Principle.


Things we need to do in this project is explained in the below mentioned link:

    http://codekata.com/kata/kata04-data-munging/


This Project consist of 3 modules:

    weather.pj : In this module the weather data is cleared, And required output is given.
    football.py : In this module the football data is cleared, And required output is given.
    common.py : This file consists of all the common operations.


INSTRUCTION REQUIRED TO RUN THIS PROJECT:

    Only Run "python3.8" to all the modules

        1. To check the weather data run weather.py
        2. To check the football data run football.py


NOTE: All the required data and project details is mentioned in http://codekata.com/kata/kata04-data-munging/
