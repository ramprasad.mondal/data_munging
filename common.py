class Extractor:
    def extract_data(self):
        """
        reading the dat file
        :return list
        """
        return list((open(self, 'r')))


class Analyser:
    """analyse the actual data"""
    def min_diff(self):
        """
        filtering data and give the minmum difference
        :return string
        """
        del self.data[0]
        filtered_data = []
        for row in self.data:
            temp = row.replace(self.rmv[0], self.rmv[1])
            if len(temp.split()):
                temp1 = temp.split()
                filtered_data.append(
                    (float(temp1[self.col[0]]),
                        float(temp1[self.col[1]]),
                        temp1[self.col[2]])
                )
            else:
                pass

        data = filtered_data
        temp = abs(data[0][0] - data[0][1])
        for data in data:
            if abs(data[0] - data[1]) < temp:
                temp = abs(data[0] - data[1])
                minimum = data

        return '{} with minimum difference of {}'.format(
            minimum[-1], abs(minimum[0]-minimum[1])
        )
