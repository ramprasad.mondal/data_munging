from common import Analyser
from common import Extractor


class WeatherAnalyser(Analyser):
    """analyse the weather data"""
    def __init__(self, file):
        self.data = Extractor.extract_data(file)
        self.rmv = ('*', ' ')
        self.col = (1, 2, 0)


weather_morristown = WeatherAnalyser("weather.dat")
print(weather_morristown.min_diff())
